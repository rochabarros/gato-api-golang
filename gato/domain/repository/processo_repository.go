package repository

import "unimedfortaleza.com.br/gato/domain/model"

type ProcessoRepository interface {
	ObterProcessoPorNumero(numero string) (*model.Processo, error)

	SalvarProcesso(*model.Processo) error
}

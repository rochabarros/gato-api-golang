package model

import "time"

type colaborador struct {
	pessoa
	matricula string
	area      areaAtuacao
}

func NewColaborador(id, nome, cpf, rg, email, celular, matricula string,
	dataNascimento time.Time, area areaAtuacao) *colaborador {

	return &colaborador{
		pessoa{id, nome, cpf, rg, email, celular, dataNascimento},
		matricula,
		area,
	}
}

func (c *colaborador) GetMatricula() string {
	return c.matricula
}

func (c *colaborador) GetAreaAtuacao() *areaAtuacao {
	return &c.area
}

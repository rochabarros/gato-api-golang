package model

import "time"

type compromisso struct {
	id            string
	dataInicio    time.Time
	dataFim       time.Time
	dataCadastro  time.Time
	dataAlteracao time.Time
	local         string
	descricao     string
	situacao      SituacaoCompromisso
	tipo          TipoCompromisso
}

func NewCompromisso(id string, dataInicio, dataFim, dataCadastro, dataAlteracao time.Time, local, descricao string,
	situacao SituacaoCompromisso, tipo TipoCompromisso) *compromisso {
	return &compromisso{id, dataInicio, dataFim, dataCadastro, dataAlteracao, local, descricao, situacao, tipo}
}

func (c *compromisso) GetId() string {
	return c.id
}

func (c *compromisso) GetDataInicio() time.Time {
	return c.dataInicio
}

func (c *compromisso) GetDataFim() time.Time {
	return c.dataFim
}

func (c *compromisso) GetDataCadastro() time.Time {
	return c.dataCadastro
}

func (c *compromisso) GetDataAlteracao() time.Time {
	return c.dataAlteracao
}

func (c *compromisso) GetLocal() string {
	return c.local
}

func (c *compromisso) GetDescricao() string {
	return c.descricao
}

func (c *compromisso) GetSituacao() SituacaoCompromisso {
	return c.situacao
}

func (c *compromisso) GetTipo() TipoCompromisso {
	return c.tipo
}

package model

type SituacaoCompromisso int

const (
	Pendente SituacaoCompromisso = iota
	Agendada
	Concluida
	Cancelada
	Reagendada
)

func (sc SituacaoCompromisso) String() string {
	return [...]string{"Pendente", "Agendada", "Concluida", "Cancelada", "Reagendada"}[sc]
}

package model

import "time"

type Processo struct {
	id               string
	numero           string
	pasta            string
	resumo           string
	dataCadastro     time.Time
	dataAlteracao    time.Time
	advogadoExterno  advogado
	adversoPrincipal pessoa
	compromissos     []compromisso
}

func NewProcesso(id, numero, pasta, resumo string, dataCadastro, dataAlteracao time.Time,
	advogadoExterno advogado, adversoPrincipal pessoa, compromissos []compromisso) *Processo {

	return &Processo{id, numero, pasta, resumo, dataCadastro, dataAlteracao,
		advogadoExterno, adversoPrincipal, compromissos}
}

func (p *Processo) GetId() string {
	return p.id
}

func (p *Processo) GetNumero() string {
	return p.numero
}

func (p *Processo) GetPasta() string {
	return p.pasta
}

func (p *Processo) GetDataCadastro() time.Time {
	return p.dataCadastro
}

func (p *Processo) GetDataAlteracao() time.Time {
	return p.dataAlteracao
}

func (p *Processo) GetAdvogadoExterno() advogado {
	return p.advogadoExterno
}

func (p *Processo) GetAdversoPrincipal() pessoa {
	return p.adversoPrincipal
}

func (p *Processo) GetCompromissos() []compromisso {
	return p.compromissos
}

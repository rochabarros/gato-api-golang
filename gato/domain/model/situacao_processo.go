package model

type SituacaoProcesso int

const (
	Ativo SituacaoProcesso = iota
	Inativo
)

func (sp SituacaoProcesso) String() string {
	return [...]string{"Ativo", "Inativo"}[sp]
}

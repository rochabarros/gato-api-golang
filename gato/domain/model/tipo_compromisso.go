package model

type TipoCompromisso int

const (
	Audiencia TipoCompromisso = iota
)

func (tc TipoCompromisso) String() string {
	return [...]string{"Audiencia"}[tc]
}

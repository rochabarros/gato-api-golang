package model

type areaAtuacao struct {
	id   string
	nome string
	cor  string
}

func NewAreaAtuacao(id, nome, cor string) *areaAtuacao {
	return &areaAtuacao{id, nome, cor}
}

func (a *areaAtuacao) GetId() string {
	return a.id
}

func (a *areaAtuacao) GetNome() string {
	return a.nome
}

func (a *areaAtuacao) GetCor() string {
	return a.cor
}

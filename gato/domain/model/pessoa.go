package model

import "time"

type pessoa struct {
	id             string
	nome           string
	cpf            string
	rg             string
	email          string
	celular        string
	dataNascimento time.Time
}

func NewPessoa(id, nome, cpf, rg, email, celular string, dataNascimento time.Time) *pessoa {
	return &pessoa{id, nome, cpf, rg, email, celular, dataNascimento}
}

func (p *pessoa) GetId() string {
	return p.id
}

func (p *pessoa) GetNome() string {
	return p.nome
}

func (p *pessoa) GetCpf() string {
	return p.cpf
}

func (p *pessoa) GetRg() string {
	return p.rg
}

func (p *pessoa) GetEmail() string {
	return p.email
}

func (u *pessoa) GetCelular() string {
	return u.celular
}

func (u *pessoa) GetDataNascimento() time.Time {
	return u.dataNascimento
}

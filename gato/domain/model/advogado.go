package model

import "time"

type advogado struct {
	pessoa
	oab string
}

func NewAdvogado(id, nome, cpf, rg, email, celular, oab string, dataNascimento time.Time) *advogado {
	return &advogado{
		pessoa{id, nome, cpf, rg, email, celular, dataNascimento},
		oab,
	}
}

func (a *advogado) GetOab() string {
	return a.oab
}
